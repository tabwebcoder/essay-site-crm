<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DiscountCodesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Discount Codes');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="card">
        <div class="card-body">

            <h1><?= Html::encode($this->title) ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        
            <p>
                <?= Html::a(Yii::t('app', 'Create Discount Codes'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
        
                    //'id',
                    'code',
                    'percent_discount',
                    'created_on:datetime',
                    ['attribute' => 'status', 'value' => function($data){return $data->status ? 'Active' : 'Disabled'; }],
        
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
    
<?php
$this->registerJs("
jQuery(document).ready(function () {
        jQuery('.grid-view').find('table').removeClass('table-striped table-bordered').addClass('color-bordered-table muted-bordered-table');
    })
");
?>
