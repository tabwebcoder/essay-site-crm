<?php

/**

 * Created by PhpStorm.

 * User: umerz

 * Date: 9/21/2018

 * Time: 3:15 PM

 */



namespace backend\util;



use common\models\Urgency;

use Yii;


class Help

{

    public static function getCustomerDealLine($urgencyId)

    {

        $urgency = Urgency::find()->where(['id' => $urgencyId])->one();

        $deadline = date('Y-m-d h:i:s a', strtotime('+ ' . $urgency->name));



        return $deadline;

    }

    public static function getCurrency($Id)

    {

        $getCurrency = (new \yii\db\Query())
        ->select('name')
        ->from('currencies')
        ->where(['id' => $Id])
        ->all();
        return $getCurrency;

    }

public static function getAllCurrency()

    {

        $currency = (new \yii\db\Query())
        ->select('id,name')
        ->from('currencies')
        ->all();
        return $currency;

    }

public static function get_progress_order($order_date,$writer_deadline){
    $start = strtotime($order_date);
    $end   = strtotime($writer_deadline);
    $current = strtotime('now');
    $completed =0;
    $progress_order ='';
    if($start <= $current){
        if($start > 0 && $end >0){
            $total =    $current - $start;
            $divide_by =    $end - $start;
            if($total > 0 && $divide_by >0){
                $completed = ($total / $divide_by) * 100;
                if($completed > 0){
                    if(round($completed) >= 50 && round($completed < 75)){
                        $progress_order = "table-info";
                    }else if(round($completed) >= 75){
                        $progress_order = "table-danger 1";
                    }else{
                         $progress_order = "table-info 2";
                    }
                }else{
                    $progress_order = "table-danger 3";
                }
            }
        }
    }
        $result['progress_order']= $progress_order;
        $result['completed']= $completed;
        return $result;
}
    public static function getRemainingDays($writerDeadline)

    {

        $date = strtotime($writerDeadline);//Converted to a PHP date (a second count)



        $diff = $date - time();//time returns current time in seconds

        $days = floor($diff / (60 * 60 * 24));//seconds/minute*minutes/hour*hours/day)

        $hours = round(($diff - $days * 60 * 60 * 24) / (60 * 60));

        return "$days days $hours hours";

    }



    public static function getToken($length)

    {

        $token = "";

        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";

        $codeAlphabet .= "0123456789";

        $max = strlen($codeAlphabet); // edited



        for ($i = 0; $i < $length; $i++) {

            $token .= $codeAlphabet[self::_crypto_rand_secure(0, $max - 1)];

        }



        return $token;

    }



    private static function _crypto_rand_secure($min, $max)

    {

        $range = $max - $min;

        if ($range < 1) return $min; // not so random...

        $log = ceil(log($range, 2));

        $bytes = (int)($log / 8) + 1; // length in bytes

        $bits = (int)$log + 1; // length in bits

        $filter = (int)(1 << $bits) - 1; // set all lower bits to 1

        do {

            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));

            $rnd = $rnd & $filter; // discard irrelevant bits

        } while ($rnd > $range);

        return $min + $rnd;

    }

}