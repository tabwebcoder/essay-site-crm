<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property string $id
 * @property string $customer_id
 * @property string $agent_id
 * @property string $writer_id
 * @property string $status
 * @property string $total_amount
 * @property string $paper_type_id
 * @property string $service_type_id
 * @property string $order_date
 * @property string $urgency_id
 * @property string $customer_deadline
 * @property string $quality_id
 * @property string $no_pages_id
 * @property string $subject_area_id
 * @property string $topic
 * @property string $acadamic_level_id
 * @property string $writing_style_id
 * @property string $language
 * @property int $referances
 * @property string $instructions
 * @property string $payment_method
 * @property string $writer_deadline
 *
 * @property Customers $customer
 * @property WritingStyles $writingStyle
 * @property User $agent
 * @property User $writer
 * @property PaperTypes $paperType
 * @property ServiceTypes $serviceType
 * @property NoPages $noPages
 * @property QualityLevels $quality
 * @property SubjectsAreas $subjectArea
 * @property AcademicLevels $acadamicLevel
 * @property Urgency $urgency
 *
 * @property Transactions[] $transactions
 * @property WriterTasks[] $writerTasks
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['total_amount', 'paper_type_id', 'service_type_id', 'order_date', 'urgency_id', 'customer_deadline', 'quality_id', 'no_pages_id', 'subject_area_id', 'topic', 'acadamic_level_id', 'writing_style_id', 'instructions', 'payment_method'], 'required'],
            [['customer_id', 'agent_id', 'writer_id', 'paper_type_id', 'service_type_id', 'urgency_id', 'quality_id', 'no_pages_id', 'subject_area_id', 'acadamic_level_id', 'writing_style_id', 'referances'], 'integer'],
            [['status', 'topic', 'instructions', 'payment_method'], 'string'],
            [['total_amount'], 'number'],
            [['order_date', 'customer_deadline', 'writer_deadline'], 'safe'],
            [['language'], 'string', 'max' => 150],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['writing_style_id'], 'exist', 'skipOnError' => true, 'targetClass' => WritingStyles::className(), 'targetAttribute' => ['writing_style_id' => 'id']],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['agent_id' => 'id']],
            [['writer_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['writer_id' => 'id']],
            [['paper_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaperTypes::className(), 'targetAttribute' => ['paper_type_id' => 'id']],
            [['service_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceTypes::className(), 'targetAttribute' => ['service_type_id' => 'id']],
            [['no_pages_id'], 'exist', 'skipOnError' => true, 'targetClass' => NoPages::className(), 'targetAttribute' => ['no_pages_id' => 'id']],
            [['quality_id'], 'exist', 'skipOnError' => true, 'targetClass' => QualityLevels::className(), 'targetAttribute' => ['quality_id' => 'id']],
            [['subject_area_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubjectsAreas::className(), 'targetAttribute' => ['subject_area_id' => 'id']],
            [['acadamic_level_id'], 'exist', 'skipOnError' => true, 'targetClass' => AcademicLevels::className(), 'targetAttribute' => ['acadamic_level_id' => 'id']],
            [['urgency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Urgency::className(), 'targetAttribute' => ['urgency_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'agent_id' => 'Agent ID',
            'writer_id' => 'Writer ID',
            'status' => 'Status',
            'total_amount' => 'Total Amount',
            'paper_type_id' => 'Paper Type',
            'service_type_id' => 'Service Type',
            'order_date' => 'Order Date',
            'urgency_id' => 'Urgency',
            'customer_deadline' => 'Customer Deadline',
            'quality_id' => 'Quality',
            'no_pages_id' => 'Num of Pages',
            'subject_area_id' => 'Subject Area',
            'topic' => 'Topic',
            'acadamic_level_id' => 'Academic Level',
            'writing_style_id' => 'Writing Style',
            'language' => 'Language',
            'referances' => 'References',
            'instructions' => 'Instructions',
            'payment_method' => 'Payment Method',
            'writer_deadline' => 'Writer Deadline',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWritingStyle()
    {
        return $this->hasOne(WritingStyles::className(), ['id' => 'writing_style_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(User::className(), ['id' => 'agent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWriter()
    {
        return $this->hasOne(User::className(), ['id' => 'writer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaperType()
    {
        return $this->hasOne(PaperTypes::className(), ['id' => 'paper_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceType()
    {
        return $this->hasOne(ServiceTypes::className(), ['id' => 'service_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoPages()
    {
        return $this->hasOne(NoPages::className(), ['id' => 'no_pages_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuality()
    {
        return $this->hasOne(QualityLevels::className(), ['id' => 'quality_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectArea()
    {
        return $this->hasOne(SubjectsAreas::className(), ['id' => 'subject_area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcadamicLevel()
    {
        return $this->hasOne(AcademicLevels::className(), ['id' => 'acadamic_level_id']);
    }

    public function getUrgency()
    {
        return $this->hasOne(Urgency::className(), ['id' => 'urgency_id']);
    }
    public function getTransactions()
    {
        return $this->hasMany(Transactions::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWriterTasks()
    {
        return $this->hasMany(WriterTasks::className(), ['order_id' => 'id']);
    }

}
