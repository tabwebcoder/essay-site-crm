<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FixedLinks;

/**
 * FixedLinksSearch represents the model behind the search form of `common\models\FixedLinks`.
 */
class FixedLinksSearch extends FixedLinks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'paper_type_id', 'service_type_id', 'urgency_id', 'quality_level_id', 'no_pages_id'], 'integer'],
            [['title', 'unique_code', 'show_discount_msg', 'status', 'expiry_date', 'currency', 'created_at'], 'safe'],
            [['amount', 'discount_percentage', 'discount_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FixedLinks::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'amount' => $this->amount,
            'discount_percentage' => $this->discount_percentage,
            'discount_amount' => $this->discount_amount,
            'expiry_date' => $this->expiry_date,
            'paper_type_id' => $this->paper_type_id,
            'service_type_id' => $this->service_type_id,
            'urgency_id' => $this->urgency_id,
            'quality_level_id' => $this->quality_level_id,
            'no_pages_id' => $this->no_pages_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'unique_code', $this->unique_code])
            ->andFilterWhere(['like', 'show_discount_msg', $this->show_discount_msg])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'currency', $this->currency]);

        return $dataProvider;
    }
}
