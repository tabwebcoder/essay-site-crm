<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FixedLinks */
/* @var $form yii\widgets\ActiveForm */
$paperTypeList = ArrayHelper::map(\common\models\PaperTypes::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$serviceTypeList = ArrayHelper::map(\common\models\ServiceTypes::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$urgencyList = ArrayHelper::map(\common\models\Urgency::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$qualityList = ArrayHelper::map(\common\models\QualityLevels::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$pagesList = ArrayHelper::map(\common\models\NoPages::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$currenciesList = ArrayHelper::map(\common\models\Currencies::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
?>
<!--<style type="text/css">
    .nav-tabs { border-bottom: 2px solid #DDD; }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
    .nav-tabs > li > a { border: none; color: #666; }
    .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
    .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
    .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
    .tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
    .tab-pane { padding: 15px 0; }
    .tab-content{padding:20px}

    .card {background: #FFF none repeat scroll 0% 0%; box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3); margin-bottom: 30px; }
</style>-->
<div class="fixed-links-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 150]) ?>

    <?= $form->field($model, 'unique_code')->textInput(['maxlength' => 150]) ?>

    <?= $form->field($model, 'paper_type_id')->dropDownList($paperTypeList,['prompt'=>'Select Paper Type']);?>

    <?= $form->field($model, 'service_type_id')->dropDownList($serviceTypeList,['prompt'=>'Select Service Type']);?>

    <?= $form->field($model, 'urgency_id')->dropDownList($urgencyList,['prompt'=>'Select Urgency']);?>

    <?= $form->field($model, 'quality_level_id')->dropDownList($qualityList,['prompt'=>'Select Quality Level']);?>

    <?= $form->field($model, 'no_pages_id')->dropDownList($pagesList,['prompt'=>'Select Num of Pages']);?>

    <?= $form->field($model, 'currency')->dropDownList($currenciesList);?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => 150]) ?>
    <div id="actual_amount_label" class="alert alert-success"></div>

    <div id="discount-section">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs customtab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Percentage</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Discount</span></a> </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="home7" role="tabpanel">
                    <div class="p-20">
                        <div class="line">
                            <div class="headbg">Discount Percentage:</div>
                            <div class="feilds"><input type="text" name="percentage_discount_percentage" class="form-control col-md-2" value="" id="discount_percentage_field" autocomplete="off" /></div>
                        </div>

                        <div class="line">
                            <div class="headbg">Discount Amount Will Be:</div>
                            <div class="feilds discount" id="percentage_discount_amount"></div>
                        </div>

                        <div class="line">
                            <div class="headbg">Charging Amount Will Be:</div>
                            <div class="feilds charge" id="percentage_discount_charge"></div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="profile7" role="tabpanel">
                    <div class="p-20">
                        <div class="line">
                            <div class="headbg">Discount Amount:</div>
                            <div class="feilds"><input type="text" name="fixed_discount_amount" value="" class="form-control col-md-2" id="discount_amount_field" autocomplete="off" /></div>
                        </div>

                        <div class="line">
                            <div class="headbg">Discount Percentage Will Be:</div>
                            <div class="feilds discount" id="fixed_discount_percentage"></div>
                        </div>

                        <div class="line">
                            <div class="headbg">Charging Amount Will Be:</div>
                            <div class="feilds charge" id="fixed_discount_charge"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'show_discount_msg')->checkbox(['class'=>'']) ?>
    <?= $form->field($model, 'expiry_date')->textInput(['class'=>'form-control dt']) ?>

    <div class="form-group">
        <input type="hidden" name="fixed_exchange_rate" value="" id="exchange_rate" />
        <input type="hidden" name="fixed_or_percentage" value="2" id="f_or_p_selected" />
        <?= Html::submitButton('Add Fixed Link', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs(
    "
    $(function () {
         $('.dt').datetimepicker();
     });"
)

?>