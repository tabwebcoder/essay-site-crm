<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "role_access".
 *
 * @property string $id
 * @property string $role_id
 * @property string $controller_id
 */
class RoleAccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role_access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id', 'controller_id'], 'required'],
            [['role_id'], 'integer'],
            [['controller_id'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => 'Role ID',
            'controller_id' => 'Controller ID',
        ];
    }
}
