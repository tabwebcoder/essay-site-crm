<?php

$roleId = Yii::$app->user->identity->role_id;

$controllerIdss = \common\models\RoleAccess::find()->select('controller_id')->where(['role_id' => $roleId])->asArray()->all();



foreach ($controllerIdss as $v)

    $currentAccess[] = $v['controller_id'];



//die;

?>

<style>

    .user-profile .dropdown-menu {

        margin: 25px auto;

    }



    .sidebar-user-name {

        font-size: 15px;

    }

</style>

<aside class="left-sidebar">

    <!-- Sidebar scroll-->

    <div class="scroll-sidebar">

        <!-- User profile -->

        <div class="user-profile">

            <!-- User profile image -->

            <div class="profile-img"><img

                        src="/<?= \Yii::$app->params['subfolder'] ?>monster-admin/assets/images/users/1.png" alt="user"/>

            </div>

            <!-- User profile text-->

            <div class="profile-text"><a href="#" class="dropdown-toggle sidebar-user-name link u-dropdown"

                                         data-toggle="dropdown" role="button" aria-haspopup="true"

                                         aria-expanded="true"><?= ucwords(Yii::$app->user->identity->full_name) ?><br/><?= date('M d,D h:i A') ?> <span

                            class="caret"></span></a>

                <div class="dropdown-menu animated flipInY">

                    <div class="dropdown-divider"></div>

                    <a href="<?= \yii\helpers\Url::to(['user/update', 'id' => Yii::$app->user->id]) ?>"

                       class="dropdown-item"><i class="ti-settings"></i> Edit Profile</a>

                    <div class="dropdown-divider"></div>

                    <a href="javascript:;" class="dropdown-item cs-logout"><i class="fa fa-power-off"></i> Logout</a>

                </div>

            </div>

        </div>

        <!-- End User profile text-->

        <!-- Sidebar navigation-->

        <nav class="sidebar-nav">

            <ul id="sidebarnav">

                <li>

                    <a class="" href="/<?= \Yii::$app->params['preLink'] ?>" aria-expanded="false">

                        <i class="mdi mdi-gauge"></i>

                        <span class="hide-menu">

                            Dashboard

                            <!--<span class="label label-rounded label-success">5

                            </span>-->

                        </span>

                    </a>

                </li>



                <?php if($roleId == 1): ?>

                <li class="">

                    <a class="has-arrow " href="#" aria-expanded="false"><i class="fa fa-cogs"></i><span

                                class="hide-menu">+ Product Settings</span></a>

                    <ul aria-expanded="false" class="collapse">

                        <li class="">

                            <a href="/<?= \Yii::$app->params['preLink'] ?>writing-styles/" class="">Writing Styles (manage)</a>

                        </li>

                        <li><a href="/<?= \Yii::$app->params['preLink'] ?>email-templates/">Email Templates(manage)</a></li>

                        <li><a href="/<?= \Yii::$app->params['preLink'] ?>service-types/">Servcie Types (manage)</a></li>

                        <li><a href="/<?= \Yii::$app->params['preLink'] ?>paper-types/">Paper Types (manage)</a></li>

                        <li><a href="/<?= \Yii::$app->params['preLink'] ?>quality-levels/">Quality Levels (manage)</a></li>

                        <li><a href="/<?= \Yii::$app->params['preLink'] ?>subjects-areas/">Subject Areas (manage)</a></li>

                        <li><a href="/<?= \Yii::$app->params['preLink'] ?>academic-levels/">Academic Levels (manage)</a></li>

                        <li><a href="/<?= \Yii::$app->params['preLink'] ?>no-pages/">Number of Pages (manage)</a></li>

                         <li><a href="/<?= \Yii::$app->params['preLink'] ?>smtp/smtp-view/">SMTP Setting</a></li>

                    </ul>

                </li>

                <?php endif; ?>

                <?php if($roleId == \common\models\User::ROLE_ADMIN || $roleId == \common\models\User::ROLE_CSR): ?>

                    <li class="">

                        <a class="has-arrow " href="#" aria-expanded="false"><i class="fa fa-shopping-cart"></i><span

                                    class="hide-menu">+ Orders</span></a>

                        <ul aria-expanded="false" class="collapse">

                            <li class="">

                                <a href="/<?= \Yii::$app->params['preLink'] ?>order/pending" class="">Pending Orders</a>

                            </li>

                            <li><a href="/<?= \Yii::$app->params['preLink'] ?>order/process">Order in Process</a></li>

                            <li class="">

                                <a class="" href="/<?= \Yii::$app->params['preLink'] ?>site/index">All Orders</a></li>



                        </ul>

                    </li>

                    <li class="">

                        <a class="has-arrow " href="#" aria-expanded="false"><i class="fa fa-anchor"></i><span

                                    class="hide-menu">+ Fixed Links</span></a>

                        <ul aria-expanded="false" class="collapse">

                            <li class="">

                                <a href="/<?= \Yii::$app->params['preLink'] ?>fixed-links/index" class="">Fixed Links List</a>

                            </li>

                            <li><a href="/<?= \Yii::$app->params['preLink'] ?>fixed-links/create">Add Fixed Links</a></li>





                        </ul>

                    </li>

                <li class="">

                    <a class="has-arrow " href="#" aria-expanded="false"><i class="fa fa-users"></i><span

                                class="hide-menu">+ USER</span></a>

                    <ul aria-expanded="false" class="collapse">

                        <li class="">

                            <a href="/<?= \Yii::$app->params['preLink'] ?>user/" class="">View Users</a>

                        </li>

                        <li><a href="/<?= \Yii::$app->params['preLink'] ?>user/create">Add User</a></li>

                    </ul>

                </li>

                <?php endif; ?>

                <li class="nav-devider"></li>

            </ul>

        </nav>

        <!-- End Sidebar navigation -->

    </div>

    <!-- End Sidebar scroll-->

    <!-- Bottom points-->

    <div class="sidebar-footer">

        <!-- item-->

        <!--<a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>-->

        <!-- item-->

        <!--<a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>-->

        <!-- item-->

        <!--<a href="javascript:;" class="link cs-logout" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>-->

    </div>

    <!-- End Bottom points-->

</aside>



<!-- #page-sidebar -->