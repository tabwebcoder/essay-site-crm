<?php
/**
 * Created using Netbeans
 * User: Awais-Mustafa
 * Date: 3/30/2020
 */

namespace backend\controllers;

use common\models\Customers;
use common\models\Orders;
use common\models\Transactions;
use common\models\User;
use common\models\PaperTypes;
use common\models\ServiceTypes;
use common\models\SubjectsAreas;
use common\models\QualityLevels;
use common\models\AcademicLevels;
use common\models\Currencies;
use common\models\WritingStyles;
use common\models\NoPages;
use common\models\Urgency;
use common\models\DiscountCodes;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;
use yii\helpers\Json;
use Yii;

class EssayApiController extends Controller{
    // Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers
     */
    Const APPLICATION_ID = 'PJSUPPORT';

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array();
    }
    
    public function actionPullDiscountCode($discountCode)
    {
        if($discountCode == 'allCodes'){
            
            $discountCodesModel = DiscountCodes::find()->where(['status' => 1])->all();
            if(count($discountCodesModel)){
                $allCodes = [];
                foreach($discountCodesModel as $code){
                    $allCodes[] = $code->code;
                }
                return ['success' => 1, 'codes' => $allCodes];
            }
            else {
                return ['success' => 0];
            }
            
        }
        else {
            
            $discountCodesModel = DiscountCodes::findOne(['code' => $discountCode]);
            if($discountCodesModel->status == 1){
                return ['success' => 1, 'code' => $discountCodesModel->code, 'discount' => $discountCodesModel->percent_discount];
            }
            else {
                return ['success' => 0];
            }
            
        }

    }
    
    public function actionPullData($tableName)
    {
        if($tableName == 'PaperTypes'){
            $tableModel = PaperTypes::find()->orderBy(['name' => SORT_ASC])->all();
        }
        else if($tableName == 'NoPages'){
            $tableModel = NoPages::find()->all();
        }
        else if($tableName == 'QualityLevels'){
            $tableModel = QualityLevels::find()->all();
        }
        else if($tableName == 'Currencies'){
            $tableModel = Currencies::find()->all();
        }
        else if($tableName == 'SubjectsAreas'){
            $tableModel = SubjectsAreas::find()->orderBy(['name' => SORT_ASC])->all();
        }
        else if($tableName == 'AcademicLevels'){
            $tableModel = AcademicLevels::find()->orderBy(['name' => SORT_ASC])->all();
        }
        else if($tableName == 'WritingStyles'){
            $tableModel = WritingStyles::find()->orderBy(['name' => SORT_ASC])->all();
        }
        else if($tableName == 'ServiceTypes'){
            $tableModel = ServiceTypes::find()->orderBy(['name' => SORT_ASC])->all();
        }
        else if($tableName == 'Urgency'){
            $tableModel = Urgency::find()->all();
        }
        return $tableModel;
        //exit;
                
        
    }
    
    // Actions
    public function actionCreate() {
        
        $this->_checkAuth();
        
        $reqResponse = [];
        
        $date = $_POST['date'];
        $txnId =  $_POST['transaction_id'];
        $paymentGateway =  $_POST['payment_method'];
        
        // Subtract time from datetime
        $timeDate = strtotime($date) - (60 * 60);

        // Date and time after subtraction
        $newDate = date("Y-m-d H:i:s", $timeDate);

        $ipBasedData = $this->getLocationInfoByIp($_POST['customer_ip_address']);
        $ipBasedCity = $ipBasedData['city'];
        $ipBasedRegion = $ipBasedData['region'];
        $ipBasedCountry = $ipBasedData['country'];
    
        //insert customer record
        $customersModel = new Customers();
        $customersModel->name = $_POST['full_name'];
        $customersModel->email = $_POST['email_address'];
        $customersModel->phone = $_POST['contact_phone'];
        $customersModel->ip = $_POST['customer_ip_address'];
        $customersModel->city = $ipBasedCity;
        $customersModel->region = $ipBasedRegion;
        $customersModel->country = $ipBasedCountry;
        $customersModel->save(false);
        
        //insert order record
        $orderModel = new Orders();
        $orderModel->customer_id = $customersModel->id;
        $orderModel->agent_id = $this->get_randon_agent();
        $orderModel->status = $_POST['status'];
        $orderModel->total_amount = $_POST['total_amount'];
        $orderModel->paper_type_id = $_POST['paper_type_id'];
        $orderModel->service_type_id = $_POST['service_type_id'];
        $orderModel->order_date = $newDate;
        $orderModel->urgency_id = $_POST['urgency_id'];
        $orderModel->quality_id = $_POST['quality_id'];
        $orderModel->no_pages_id = $_POST['no_pages_id'];
        $orderModel->subject_area_id = $_POST['subject_area_id'];
        $orderModel->topic = $_POST['topic'];
        $orderModel->acadamic_level_id = $_POST['acadamic_level_id'];
        $orderModel->writing_style_id = $_POST['writing_style_id'];
        $orderModel->language = $_POST['language'];
        $orderModel->plagiarism_report = $_POST['plagiarism_report'];
        $orderModel->currency = $_POST['currency'];
        $orderModel->referances = '';
        $orderModel->instructions = $_POST['instructions'];
        $orderModel->payment_method = $paymentGateway;
        $customerDeadLine = $this->get_customer_deadline($_POST['urgency_id']);
        $orderModel->customer_deadline = $customerDeadLine;
        $orderModel->writer_deadline = $this->get_writer_deadline($customerDeadLine);
        if($orderModel->save(false)){
            
            //insert transaction record
            $transactionModel = new Transactions();
            $transactionModel->order_id = $orderModel->id;
            $transactionModel->customer_id = $customersModel->id;
            $transactionModel->payment_method = $paymentGateway;
            $transactionModel->transaction_id = $txnId;
            $transactionModel->payee_name = $_POST['full_name'];
            $transactionModel->payee_amount = $_POST['total_amount'];
            $transactionModel->fraud_status = 'Not Received';
            $transactionModel->payment_date = $date;
            $transactionModel->details = '';
            $transactionModel->status = 'Paid';
            $transactionModel->save(false);
            
            $attachmentsList = [];
            if(!empty($_POST['files_list'])){
                $files_list = explode(',', $_POST['files_list']);
                foreach ($files_list as $key => $file_list) {
                    
                    $file_list_link = 'https://essaysnassignments.com/wp-content/themes/Avada/ajax/' . $file_list;
                    
                    //insert order attachments
                    $orderAttachmentModel = new OrderAttachments();
                    $orderAttachmentModel->file_path = $file_list_link;
                    $orderAttachmentModel->order_id = $orderModel->id;
                    $orderAttachmentModel->save(FALSE);
                    
                    $attachmentsList[] = 'Attachment' . $key . ' ' . $file_list_link;
                    
                }
            }
            
            if(count($attachmentsList)){
                $attachmentsList = implode(", ", $attachmentsList);
                $attachmentsListEmail = str_replace(",", '<br>', $attachmentsList);   
            }
            
            //send email            
            $emailTempModels = \common\models\EmailTemplates::find()->where(['type' => 'client_intro'])->all();
            
            foreach($emailTempModels as $eTemplate): 
                
                $emailSubject = $eTemplate->subject;
                $emailBody = $eTemplate->body;

                $emailBody = str_replace("#Payment_gateway", $paymentGateway, $emailBody);
                $emailBody = str_replace("#agent_name", $orderModel->agent->full_name, $emailBody);
                $emailBody = str_replace("#order_no", $orderModel->id, $emailBody);
                $emailBody = str_replace("#task_topic", $orderModel->topic, $emailBody);
                $emailBody = str_replace("#client_name", $customersModel->name, $emailBody);
                $emailBody = str_replace("#no_pages", $orderModel->noPages->name, $emailBody);
                $emailBody = str_replace("#subject_area", $orderModel->subjectArea->name, $emailBody);
                $emailBody = str_replace("#order_date", $orderModel->order_date, $emailBody);
                $emailBody = str_replace("#paper_types", $orderModel->paperType->name, $emailBody);
                $emailBody = str_replace("#service_types", $orderModel->serviceType->name, $emailBody);
                $emailBody = str_replace("#customer_deadline", $orderModel->customer_deadline, $emailBody);
                $emailBody = str_replace("#quality_levels", $orderModel->quality->name, $emailBody);
                $emailBody = str_replace("#amount", $orderModel->total_amount, $emailBody);
                
                $reqResponse['customer_emails'][] = [
                    'emailBody' => $emailBody,
                    'emailSubject' => $emailSubject,
                ];
                
            endforeach;
            
//            $emailTempPaymentRecModel = \common\models\EmailTemplates::findOne(['type' => 'payment_received']);
//            $emailSubject = $emailTempPaymentRecModel->subject;
//            $emailBody = $emailTempPaymentRecModel->body;
//            
//            $emailBody = str_replace("#Payment_gateway", $paymentGateway, $emailBody);
//            $emailBody = str_replace("#order_no", $orderModel->id, $emailBody);
//            $emailBody = str_replace("#task_topic", $orderModel->topic, $emailBody);
//            $emailBody = str_replace("#client_name", $customersModel->name, $emailBody);
//            $emailBody = str_replace("#no_pages", $orderModel->noPages->name, $emailBody);
//            $emailBody = str_replace("#subject_area", $orderModel->subjectArea->name, $emailBody);
//            $emailBody = str_replace("#order_date", $orderModel->order_date, $emailBody);
//            $emailBody = str_replace("#paper_types", $orderModel->paperType->name, $emailBody);
//            $emailBody = str_replace("#service_types", $orderModel->serviceType->name, $emailBody);
//            $emailBody = str_replace("#customer_deadline", $orderModel->customer_deadline, $emailBody);
//            $emailBody = str_replace("#quality_levels", $orderModel->quality->name, $emailBody);
//            $emailBody = str_replace("#amount", $orderModel->total_amount, $emailBody);
//
            //$this->sendEmail($customersModel->email, $emailBody, $emailSubject, '');
            
            
            ///alert to admin
            $adminNotifyEmailTempModel = \common\models\EmailTemplates::findOne(['type' => 'notify']);
            
            $adminEmailSubject = $adminNotifyEmailTempModel->subject;
            $adminEmailSubject = str_replace("#order_id", $orderModel->id, $adminEmailSubject);
            $adminEmailSubject = str_replace("#no_pages", $orderModel->noPages->name, $adminEmailSubject);
            $adminEmailSubject = str_replace("#urgency", $orderModel->urgency->name, $adminEmailSubject);
            
            $writerDeadline = $orderModel->writer_deadline;
            $customerDeadline = $orderModel->customer_deadline;
            
            //admin email body
            $adminEmailBody = $adminNotifyEmailTempModel->body;

            $adminEmailBody = str_replace("#order_id", $orderModel->id, $adminEmailBody);
            $adminEmailBody = str_replace("#no_pages", $orderModel->noPages->name, $adminEmailBody);
            $adminEmailBody = str_replace("#urgency", $orderModel->urgency->name, $adminEmailBody);

            $adminEmailBody = str_replace("#delivery_date", $orderModel->customer_deadline, $adminEmailBody);
            $adminEmailBody = str_replace("#delivery_writer", $orderModel->writer_deadline, $adminEmailBody);

            $adminEmailBody = str_replace("#paper_types", $orderModel->paperType->name, $adminEmailBody);
            $adminEmailBody = str_replace("#service_types", $orderModel->serviceType->name, $adminEmailBody);
            $adminEmailBody = str_replace("#topic", $orderModel->topic, $adminEmailBody);
            $adminEmailBody = str_replace("#quality_levels", $orderModel->quality->name, $adminEmailBody);
            $adminEmailBody = str_replace("#no_pages", $orderModel->noPages->name, $adminEmailBody);
            $adminEmailBody = str_replace("#academic_levels", $orderModel->acadamicLevel->name, $adminEmailBody);
            $adminEmailBody = str_replace("#subjects_areas", $orderModel->subjectArea->name, $adminEmailBody);
            $adminEmailBody = str_replace("#writing_styles", $orderModel->writingStyle->name, $adminEmailBody);
            $adminEmailBody = str_replace("#language", $orderModel->language, $adminEmailBody);
            $adminEmailBody = str_replace("#attachments_list", $attachmentsList, $adminEmailBody);

            $adminEmailBody = str_replace("#payment_method", $paymentGateway, $adminEmailBody);
            $adminEmailBody = str_replace("#amount", $orderModel->total_amount, $adminEmailBody);
            $adminEmailBody = str_replace("#currency", $orderModel->currency, $adminEmailBody);
            
            if ($orderModel->plagiarism_report == 1) {
                $preport = 'Yes';
            } else {
                $preport = 'No';
            }
            
            $adminEmailBody = str_replace("#plagiarism_report", $preport, $adminEmailBody);
            $adminEmailBody = str_replace("#customer_name", $customersModel->name, $adminEmailBody);
            $adminEmailBody = str_replace("#customer_email", $customersModel->email, $adminEmailBody);
            $adminEmailBody = str_replace("#customer_phone", $customersModel->phone, $adminEmailBody);
            $adminEmailBody = str_replace("#customer_ip", $customersModel->ip, $adminEmailBody);
            $adminEmailBody = str_replace("#city", $customersModel->city, $adminEmailBody);
            $adminEmailBody = str_replace("#region", $customersModel->region, $adminEmailBody);
            $adminEmailBody = str_replace("#country", $customersModel->country, $adminEmailBody);
            
            //$this->sendEmail('info@essaysnassignments.co.uk',$adminEmailBody, $adminEmailSubject);            
            $reqResponse['email_details'] = [
                'emailTo' => $customersModel->email,
                'adminEmailBody' => $adminEmailBody,
                'adminEmailSubject' => $adminEmailSubject,
                'adminEmailTo' => 'info@essaysnassignments.co.uk',
                'adminEmailBcc' => $adminNotifyEmailTempModel->bcc,
                'orderId' => $orderModel->id
            ];  
            
            return $reqResponse;
            
        }       
        

    }

    protected function sendEmail($emailTo='', $emailBody, $emailSubject, $emailFrom = 'notifications@usjobhelpcenter.com') {
        
        $headers = "From: " . strip_tags($emailFrom) . "\r\n";
        $headers .= "Reply-To: no-reply@essaysnassignments.co.uk\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        mail($emailTo, $emailSubject, $emailBody, $headers);
        
        // $mailer = Yii::$app->mailer->compose();
        // $mailer->setHtmlBody($emailBody);
        // $mailer->setFrom($emailFrom)
        //         ->setTo($emailTo)
        //         ->setSubject($emailSubject)
        //         ->send();
        
    }
    
    public function actionFixedLinks(){
        
        $sqlFixedLinks = "select fixed_links.*,paper_types.name as paper_types_name,quality_levels.name as quality_levels_name,service_types.name as service_types_name,currencies.name as currencies_name,urgency.name as urgency_name,no_pages.name as no_pages_name from  fixed_links
            JOIN paper_types ON paper_types.id = fixed_links.paper_type_id join quality_levels on quality_levels.id = fixed_links.quality_level_id JOIN service_types ON service_types.id = fixed_links.service_type_id
            JOIN currencies ON currencies.id = fixed_links.currency
            JOIN urgency on urgency.id = fixed_links.urgency_id JOIN no_pages on no_pages.id = fixed_links.no_pages_id where unique_code = '".$_POST['code']."'";
        
        $resultFixedLinks = Yii::$app->db->createCommand($sqlFixedLinks)->queryOne();
        
        return $resultFixedLinks;
        
        
    }
    
    public function getLocationInfoByIp($userIp) {
        
        $result = array('country' => '', 'city' => '');
        $ip = $userIp;
        $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if ($ip_data && $ip_data->geoplugin_countryName != null) {
            $result['country'] = $ip_data->geoplugin_countryCode;
            $result['city'] = $ip_data->geoplugin_city;
            $result['region'] = $ip_data->geoplugin_regionName;
        }
        return $result;
        
    }
    
    public function get_customer_deadline($urgencyId){
        
        $urgencyModel = \common\models\Urgency::findOne($urgencyId);
        return  $deadline = date('Y-m-d h:i:s', strtotime('+'.$urgencyModel->name));
        
    }
    
    public function get_writer_deadline($deadline){
        $startdate = date("Y-m-d H:i:s");
        $enddate   = $deadline; // 
        $d1= new \DateTime($startdate); 
        $d2= new \DateTime($enddate);
        $interval= $d1->diff($d2);
        $days = ($interval->days*24) + $interval->h;
        $days_25 =  $days/100*25;
        $hours_calculate =round($days-$days_25);
        return $new_time = date("Y-m-d H:i:s", strtotime('+'.$hours_calculate.' hours'));
    }
    
    public function get_randon_agent(){
        
        $randomAgentModel = Yii::$app->db->createCommand('SELECT * FROM user where role_id = "2" ORDER BY rand()')->queryOne(); 
        return $randomAgentModel['id'];
        
    }
    
    public function getRemainingDays($writerDeadline)
    {
        $date = strtotime($writerDeadline);//Converted to a PHP date (a second count)
        $diff = $date - time();//time returns current time in seconds
        $days = floor($diff / (60 * 60 * 24));//seconds/minute*minutes/hour*hours/day)
        $hours = round(($diff - $days * 60 * 60 * 24) / (60 * 60));
        return "$days days $hours hours";
    }
    

    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if($body != '')
        {
            // send the body
            echo $body;
        }
        // we need to create the body if none is passed
        else
        {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch($status)
            {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templated in a real-world solution
            $body = '
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
                </head>
                <body>
                    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
                    <p>' . $message . '</p>
                    <hr />
                    <address>' . $signature . '</address>
                </body>
                </html>';

            echo $body;
        }
        Yii::$app->end();
    }

    private function _getStatusCodeMessage($status) {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    private function _checkAuth() {
        // Check if we have the USERNAME and PASSWORD HTTP headers set?
        if(!(isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW']))) {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];
        // Find the user
        $user = User::findByUsername(strtolower($username));

        if($user===null) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: Username or password is invalid');
        } else if(!$user->validatePassword($password)) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: Username or Password is invalid');
        }
    }

    private function _checkModel() {
        if(!isset($_REQUEST['model'])) {
            $this->_sendResponse(501,
                sprintf('Fatal error: Name of the model must be provided'));
            Yii::$app->end();
        }
    }
}