<?php


use yii\helpers\Url;
$currentAccess = [];

?>
<style>
    .help-block{
        color: red;
    }
    html body .m-t-40 {
        margin-top: 0px;
    }
    .page-titles .breadcrumb .breadcrumb-item + .breadcrumb-item::before {
        content: "\e649";
        font-family: themify;
        color: #a6b7bf;
        font-size: 10px;
    }
</style>
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="/">
                <!-- Logo icon -->
                <b>
                    <i class="wi wi-sunset"></i> mhrwriter
                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto mt-md-0 ">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>

            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">
                <!--<li class="nav-item hidden-sm-down">
                    <form class="app-search">
                        <input type="text" class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a> </form>
                </li>-->

                <?php
                if( isset(Yii::$app->user->identity->full_name) ){
                    ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/<?= \Yii::$app->params['subfolder'] ?>monster-admin/assets/images/users/1.png" alt="user" class="profile-pic" /></a>
                        <div class="dropdown-menu dropdown-menu-right animated flipInY">
                            <ul class="dropdown-user">
                                <li>
                                    <div class="dw-user-box">
                                        <div class="u-img"><img src="/<?= \Yii::$app->params['subfolder'] ?>monster-admin/assets/images/users/1.png" alt="user"></div>
                                        <div class="u-text">
                                            <h4><?= isset(Yii::$app->user->identity->full_name) ? ucwords(Yii::$app->user->identity->full_name) : ''  ?></h4>
                                            <p class="text-muted"><?= isset(Yii::$app->user->identity->username) ? Yii::$app->user->identity->username : '' ?></p><a href="<?= Url::to(['user/update', 'id' => Yii::$app->user->id])?>" class="btn btn-rounded btn-danger btn-sm">Change Detail</a></div>
                                    </div>
                                </li>
                                <li role="separator" class="divider"></li>
                                <!--<li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>-->
                                <li role="separator" class="divider"></li>
                                <li class="cs-logout"><a href="javascript:;"><i class="fa fa-power-off "></i> Logout</a></li>

                            </ul>
                        </div>
                    </li>
                <?php
                }
                ?>


            </ul>
        </div>
    </nav>
</header>