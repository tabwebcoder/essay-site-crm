<?php

namespace backend\controllers;

use common\models\Customers;
use common\models\EmailTemplates;
use common\models\Transactions;
use common\models\User;
use common\models\WriterTasks;
use Yii;
use common\models\Orders;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class SmtpController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionSmtpView()
    {
       $smtp = (new \yii\db\Query())
    ->select('*')
    ->from('Smtp_settings')
    ->all();
        if ($smtp) {
            return $this->render('smtp-view', ['smtp' => $smtp[0]]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEdit($id)
    {

        $smtp = (new \yii\db\Query())
     ->select('*')
    ->from('Smtp_settings')
    ->where(['id' => $id])
    ->one();
        if ($_POST) {
            $host       =   $_POST["host"];
            $username   =   $_POST["username"];
            $password   =   $_POST["password"];
            $port       =    $_POST["port"];
            $SMTPSecure =    $_POST["SMTPSecure"];
            $sendto     =    $_POST["sendto"];
            $sendfrom   =    $_POST["sendfrom"];
            $status     =    $_POST["status"];
                $update =Yii::$app->db->createCommand()
                ->update('Smtp_settings',array('host'=>$host,'username'=> $username,'password'=>$password,'Port'=>$port,'SMTPSecure'=>$SMTPSecure,'sendto'=>$sendto,'sendfrom'=>$sendfrom,'status'=>$status), ['id' => $id])
                ->execute();
          
           return $this->redirect(['/smtp/edit?id='.$id]);
        }

        return $this->render('edit', [
            'model' => $smtp,
        ]);
    }

    public function actionPending()
    {
        return $this->render('pending');
    }

    public function actionProcess()
    {
        return $this->render('process');
    }

}
