/**
 * Created by user on 6/21/2018.
 */
if ($('.select2')[0]) {
    $(".select2").select2();
}
if($('.start_datetime')[0]){
    $(".start_datetime").datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
        startDate: new Date(),
        todayBtn: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.end_datetime').datetimepicker('setStartDate', minDate);
    });
}
if($('.end_datetime')[0]){
    $(".end_datetime").datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        startDate: new Date(),
        autoclose: true,
        todayBtn: true,
        minDate: 0
    });

}

$(".add-dm-sku").on('click',function () {
    var len = $('.dm-multi-sku tr').length;
    var skulen = len / 2;
    /*if (skulen >= requestedSKUS){
        $(this).attr('disabled',true);
        alert("You cannot request more then "+requestedSKUS+" SKUs.")
        return false;
    }*/

    //reasons array
    var reasons = ['Competitor Top', 'Focus SKUs','Philips Campaign',
        'Flash Sale','Shocking Deal','Aging Stocks', 'EOL','Competitive Pricing',
        'Outright','Others'];

    var sku = $(".multi-select-sku").val();
    var skuName = $(".multi-select-sku option:selected").text();
    var price = $(".dm-price").val();
    var qty = $(".dm-qty").val();
    var subsidy = $(".dm-subsidy").val();
    if(subsidy == '') {
        $(".dm-subsidy").val('0');
        subsidy = '0';
    }if(qty == '') {
        $(".dm-qty").val('1');
        qty = '1';
    }
    var reason = $(".dm-reason").val();
    var margin_per = $(".dm-margin-per").val();
    var margin_rm = $(".dm-margin-rm").val();
    var skuExistsCount = 0;
    $('input.skunames').each(function () {
        var skuExists = $(this).val();
        if(skuName == skuExists)
            skuExistsCount = 1;
    });
    var marginPer = margin_per.replace("%", "");
    marginPer = marginPer.replace(",", "");
    if(skuExistsCount == 0)
    {
        if(sku != '' && price != '' && qty != '' && reason != '' && isNumber(price) && isNumber(qty) && isNumber(subsidy))
        {
            if(checkApproveSku() == 'yes') {
                //if(marginPer < 0) {


                    var options = '';
                    for (var i = 0; i < reasons.length; i++) {
                        selected = (reason == reasons[i] ) ? 'selected' : '';
                        options += "<option " + selected + " value='" + reasons[i] + "'>" + reasons[i] + "</option>";
                    }

                    var html = "<td><a style='color: #54667a;' href='javascript:;' data-sku-id='" + sku + "'  class='dm-more up'><i class='mdi mdi-plus-circle-outline' data-toggle='tooltip' title='More info' style='font-size: 29px;'></i></a></td>"
                        +"<td><input type='text' class='skunames form-control' data-sku-id='" + sku + "' name='DM[s_" + sku + "][sku]' readonly value='" + skuName + "'></td> "
                        + "<td><input type='text' class=' form-control' name='DM[s_" + sku + "][price]'  value='" + price + "'></td> "
                        + "<td><input type='text' class=' form-control' name='DM[s_" + sku + "][subsidy]'  value='" + subsidy + "'></td> "
                        + "<td><input type='text' class=' form-control' name='DM[s_" + sku + "][qty]'  value='" + qty + "'></td> "
                        + "<td><input type='text' readonly class='form-control' name='DM[s_" + sku + "][margin_per]'  value='" + margin_per + "'></td> "
                        + "<td><input type='text' readonly class='form-control' name='DM[s_" + sku + "][margin_rm]'  value='" + margin_rm + "'></td> "
                        + "<td><select  class='form-control' name='DM[s_" + sku + "][reason]' >" + options + "</select></td>"
                        + "<td>&nbsp;<a href='javascript:;'  data-sku-id='" + sku + "' class='dm-delete'><i class='glyph-icon icon-trash' style='font-size:29px;color: red;'></i></a> </td> ";

                /* var more = "<td colspan='11'>"
                 + "<div class='col-md-12' >\n" +
                 "\t<div class='col-md-4'> <div class=\"panel panel-default dl-more-card\">\n" +
                 "                                        <div class=\"panel-body\">\n" +

                 "\t\t<table>\n" +
                 "\t\t\t<tr><td class='dl-more-td'><label class='small text-muted pr-2'>Actual Cost</label></td><td class='dl-more-td'><b><label class='aprice_" + sku + "'></label></b></td></tr>\n" +
                 "\t\t\t<tr><td class='dl-more-td'><label class='small text-muted pr-2'>Deal Cost</label></td><td class='dl-more-td'><b><label  class='price_" + sku + "'></label></b></td></tr>\n" +
                 "\t\t</table>\n" +
                 "\t</div></div></div>\n" +
                 "\n" +
                 "\t<div class='col-md-4'> <div class=\"panel panel-default dl-more-card\">\n" +
                 "                                        <div class=\"panel-body\">\n" +
                 "\t\t<table>\n" +
                 "\t\t\t<tr><td class='dl-more-td'><label class='small text-muted pr-2'>Commission</label></td><td class='dl-more-td'><b><label class='commision_" + sku + "'></label></b></td></tr>\n" +
                 "\t\t\t<tr><td class='dl-more-td'><label class='small text-muted pr-2'>Shipping</label></td><td class='dl-more-td'> <b><label class='shipping_" + sku + "'></label></b></td></tr>\n" +
                 "\t\t\t<tr><td class='dl-more-td'><label class='small text-muted pr-2'>Gross Profit</label> </td><td class='dl-more-td'><b><label class='gp_" + sku + "'></label></b></td></tr>\n" +
                 "\t\t\t<tr><td class='dl-more-td'><label class='small text-muted pr-2'>Price w/ subsidy</label> </td><td class='dl-more-td'><b><label class='pws_" + sku + "'></label></b></td></tr>\n" +
                 "\t\t\t<tr><td class='dl-more-td'><label class='small text-muted pr-2'>Customer Pays</label></td><td class='dl-more-td'> <b><label class='cp_" + sku + "'></label></b></td></tr>\n" +
                 "\t\t</table>\n" +
                 "\t</div></div></div>\n" +
                 "</div>\n"
                 + "</td>";*/
                var more = "<td colspan='11' style='background-color: #f5f5f5;'>\n" +
                    "<div class='row'>"+
                    "<div class='col-lg-4 col-sm-12'>"+
                    "<div class='card' style='margin-bottom: 3px'>"+
                    "<div class='card-body dt-widgets' style='height: 408px;'>"+

                "<div class='table-responsive'>"+
                "<table class='display nowrap table table-hover table-striped table-bordered dataTable no-footer'>"+
                "<tr><td class='dl-more-td'><label  >Actual Cost</label></td><td class='dl-more-td'><strong class='aprice_" + sku + "'></strong></td></tr>"+
                    "<tr><td class='dl-more-td'><label  >Deal Cost</label></td><td class='dl-more-td'><strong class='price_" + sku + "'></strong></td></tr>"+
                "</table>"+
                "</div>"+
                "</div>"+
                "   </div>"+
                "   </div>"+
                "   <div class='col-lg-4 col-sm-12'>"+
                "   <div class='card' style='margin-bottom: 3px'>"+
                "   <div class='card-body dt-widgets' style='height: 408px;'>"+
                "   <div class='table-responsive'>"+
                "   <table class='display nowrap table table-hover table-striped table-bordered dataTable no-footer'>"+
                "   <tr><td class='dl-more-td'><label  >Commission</label></td><td class='dl-more-td'><strong class='commision_" + sku + "'></strong></td></tr>"+
                "   <tr><td class='dl-more-td'><label  >Shipping</label></td><td class='dl-more-td'> <strong class='shipping_" + sku + "'></strong></td></tr>"+
                "   <tr><td class='dl-more-td'><label  >Gross Profit</label> </td><td class='dl-more-td'><strong class='gp_" + sku + "'></strong></td></tr>"+
                "   <tr><td class='dl-more-td'><label  >Price w/ subsidy</label> </td><td class='dl-more-td'><strong class='pws_" + sku + "'></strong></td></tr>"+
                "   <tr><td class='dl-more-td'><label  >Customer Pays</label></td><td class='dl-more-td'> <strong class='cp_" + sku + "'></strong></td></tr>"+
                "   </table>"+
                "   </div>"+
                "   </div>"+
                "   </div>"+
                "   </div>"+
                "   <div class='col-lg-4 col-sm-12'>"+
                "   <div class='card' style='margin-bottom: 3px'>"+
                "   <div class='card-body dt-widgets' style='height: 408px;'>"+

                "                    <div class='table-responsive'>"+
                "   <table class='display nowrap table table-hover table-striped table-bordered dataTable no-footer'>"+
                "   <tr><td class='dl-more-td'><label  >Total Stocks</label></td><td class='dl-more-td'><strong class='stocks_" + sku + "'></strong></td></tr>"+
                "   <tr><td class='dl-more-td'><label  >ISIS Stocks</label></td><td class='dl-more-td'> <strong class='isis_" + sku + "'></strong></td></tr>"+
                "   <tr><td class='dl-more-td'><label  >Office Stocks</label> </td><td class='dl-more-td'><strong class='os_" + sku + "'></strong></td></tr>"+
                "   <tr><td class='dl-more-td'><label  >FBL Blip Stocks</label> </td><td class='dl-more-td'><strong class='fbs_" + sku + "'></strong></td></tr>"+
                "   <tr><td class='dl-more-td'><label  >FBL 909 Stocks </label></td><td class='dl-more-td'> <strong class='f9s_" + sku + "'></strong></td></tr>"+
                "   <tr><td class='dl-more-td'><label  >FBL Deal4U Stocks </label></td><td class='dl-more-td'> <strong class='fds_" + sku + "'></strong></td></tr>"+
                "   <tr><td class='dl-more-td'><label  >FBL Avent Stocks </label></td><td class='dl-more-td'> <strong class='fas_" + sku + "'></strong></td></tr>"+
                "   </table>"+
                "   </div>"+
                "   </div>"+
                "   </div>"+
                "   </div>"+

                "   </div>"+
                    "</td>";



                    $(".dm-multi-sku tr:first").after("<tr  class='row-" + sku + "'>" + html + "</tr><tr class='more-" + sku + " hide'>" + more + "</tr>");

//            $(".multi-select-sku").clear();
                    $(".dm-price").val("");
                    $(".dm-qty").val("");
                    $(".dm-reason").val("");
                    $(".dm-margin-per").val("");
                    $(".dm-margin-rm").val("");
                    $(".dm-subsidy").val("");

                    $(".dm-multi-sku input").each(function () {
                        $(this).on('change', function () {
                            var dynamicSku = "DM[s_" + sku + "]";
                            calculateSku($(this), dynamicSku);
                        })
                    });
                /*} else {
                    alert(skuName + ' cannot be added as its margin are positives');
                }*/
            } else {
                alert(skuName + ' already Approved state with in start and end dates.')
            }

        }else {
            alert("Values cannot be blank or string");
        }
    } else {
        alert(skuName + " already exists!!");
    }

});

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
function checkApproveSku()
{
    var ret = '';
    var channel = $("#dealsmaker-channel_id").val();
    var start_date = $("#dealsmaker-start_date").val();
    var end_date = $("#dealsmaker-end_date").val();
    var sku = $(".multi-select-sku").val();
    $.ajax({
        async: false,
        type: "post",
        url: "/deals-maker/check-approved-sku",
        data: {'sku':sku,'channel':channel,'start_date':start_date,'end_date':end_date},
        dataType: "json",
        beforeSend: function () {},
        success: function (data) {
            ret =   data.allow;
        },
    });
    return ret;
}
$(".dm-multi-sku").on('click','a.dm-delete',function () {
    var sku = $(this).attr("data-sku-id");
    var result = confirm("Want to delete this SKU?");
    if (result) {
        $(this).closest('tr').remove();
        $('.more-'+sku).remove();
    }
});

$(".dm-multi-sku").on('click','a.dm-more',function () {
    var sku = $(this).attr("data-sku-id");
    if($(this).hasClass('up'))
    {
        $(this).removeClass("up").addClass("down");
        $('tr.more-'+sku).removeClass('hide');
        $("i", this).removeClass("mdi mdi-plus-circle-outline").addClass("mdi mdi-minus-circle-outline");
        var dynamicSku  = "DM[s_"+sku+"]";
        calculateSku($(this),dynamicSku);
    } else {
        $(this).removeClass("down").addClass("up");
        $('tr.more-'+sku).addClass('hide');
        $("i", this).removeClass("mdi mdi-minus-circle-outline").addClass("mdi mdi-plus-circle-outline");
    }


});
function calculateSku(obj,isDyn)
{
    if(isDyn == 0)
    {
        var channel = $("#dealsmaker-channel_id").val();
        var sku = $("#multi-sku-sel").val();
        var price = $("#dealsmaker-deal_price").val();
        var subsidy = $("#dealsmaker-deal_subsidy").val();
        var qty = $("#dealsmaker-deal_qty").val();
    } else {
        var channel = $("#dealsmaker-channel_id").val();
        var sku = $("input[name='"+isDyn+"[sku]']").attr("data-sku-id");

        var price = $("input[name='"+isDyn+"[price]']").val();
        var subsidy =$("input[name='"+isDyn+"[subsidy]']").val();
        var qty = $("input[name='"+isDyn+"[qty]']").val();
    }

    if(channel > 0)
    {
        $.ajax({
            async: true,
            type: "post",
            url: "/deals-maker/calculate",
            data: {'sku_id':sku,'channel':channel,price:price,subsidy:subsidy,qty:qty,fbl:0},
            dataType: "json",
            beforeSend: function () {},
            success: function (data) {
                if(isDyn == 0)
                {
                    $(obj).closest('tr').find(".dm-margin-per").val(data.sales_margins);
                    $(obj).closest('tr').find(".dm-margin-rm").val(data.sales_margins_rm);
                } else {
                    $(obj).closest('tr').find("input[name='"+isDyn+"[margin_per]']").val(data.sales_margins);
                    $(obj).closest('tr').find("input[name='"+isDyn+"[margin_rm]']").val(data.sales_margins_rm);
                }
                // add data into more row columns
                $(".aprice_"+sku).html("RM "+data.actual_cost);
                $(".price_"+sku).html("RM "+data.cost);
                $(".commision_"+sku).html(data.commission);
                $(".shipping_"+sku).html("RM "+data.shipping);
                $(".gp_"+sku).html("RM " +data.gross_profit);
                $(".pws_"+sku).html("RM "+data.price_after_subsidy);
                $(".cp_"+sku).html(data.customer_pays);
                $(".stocks_"+sku).html(data.stocks.current_stocks);
                $(".isis_"+sku).html(data.stocks.isis_stocks);
                $(".os_"+sku).html(data.stocks.office_stocks);
                $(".fbs_"+sku).html(data.stocks.fbl_stock);
                $(".f9s_"+sku).html(data.stocks.fbl_99_stock);
                $(".fds_"+sku).html(data.stocks.fbl_d4u_stock);
                $(".fas_"+sku).html(data.stocks.fbl_pavent_stock);

            },
        });
    } else {
        alert("Please select Channel");
    }

}
$(".dm-multi-sku input").each(function () {
    $(this).on('change',function () {
        var sku = $(this).attr('data-sku-id');
        if (typeof sku !== typeof undefined && sku !== false) {
            var dynamicSku  = "DM["+sku+"]";
            calculateSku($(this),dynamicSku);
        } else {
            calculateSku($(this),0);
        }

    })
});
if ( $('.dlm-admin-skus-2').length > 0 )
{
    $('.dlm-admin-skus-2').DataTable(
        {
        "order": [[ 5, "asc" ]],
        "searching": true,
        "pageLength": 25,
        "bFilter" : false,
        "bLengthChange": false
        }
    );
}