<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FixedLinks */

$this->title = 'Add Fixed Links';
$this->params['breadcrumbs'][] = ['label' => 'Fixed Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">


        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
