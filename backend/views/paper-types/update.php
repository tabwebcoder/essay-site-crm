<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaperTypes */

$this->title = 'Update Paper Types: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Paper Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="card">
    <div class="card-body">


        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
