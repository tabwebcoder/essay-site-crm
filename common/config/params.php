<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'subfolder'=>'wpanel/backend/web/',
    'preLink'=>'wpanel/backend/web/index.php/'
];
