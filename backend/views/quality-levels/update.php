<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\QualityLevels */

$this->title = 'Update Quality Levels: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Quality Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="card">
    <div class="card-body">


        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>

