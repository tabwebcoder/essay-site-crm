<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'SMTP Information #'.$model['id'];
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = $this->title;

// $subjectAreaList = ArrayHelper::map(\common\models\SubjectsAreas::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
// $academicList = ArrayHelper::map(\common\models\AcademicLevels::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
// $writingStyleList = ArrayHelper::map(\common\models\WritingStyles::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');


?>
<div class="card">
    <div class="card-body">
        <div class="writing-styles-form">

    <form id="w0" action="/<?= \Yii::$app->params['preLink'] ?>smtp/edit?id=1" method="post">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
    <input type="hidden" name="id" value="<?php echo $model['id']?>">
    <div class="form-group field-writingstyles-name">
<label class="control-label" for="writingstyles-name">Host</label>
<input type="text" id="host" class="form-control" name="host" value="<?php echo $model['host']?>" maxlength="25" aria-required="true" aria-invalid="false">
<label class="control-label" for="writingstyles-name">Username</label>
<input type="text" id="host" class="form-control" name="username" value="<?php echo $model['username']?>" maxlength="25" aria-required="true" aria-invalid="false">
<label class="control-label" for="writingstyles-name">Password</label>
<input type="text" id="host" class="form-control" name="password" value="<?php echo $model['password']?>" maxlength="25" aria-required="true" aria-invalid="false">
<label class="control-label" for="writingstyles-name">Port</label>
<input type="text" id="port" class="form-control" name="port" value="<?php echo $model['Port']?>" maxlength="25" aria-required="true" aria-invalid="false">
<label class="control-label" for="writingstyles-name">SMTP Secure</label>
<input type="text" id="host" class="form-control" name="SMTPSecure" value="<?php echo $model['SMTPSecure']?>" maxlength="25" aria-required="true" aria-invalid="false">
<label class="control-label" for="writingstyles-name">Send From</label>
<input type="text" id="sendfrom" class="form-control" name="sendfrom" value="<?php echo $model['sendfrom']?>" maxlength="25" aria-required="true" aria-invalid="false">
<label class="control-label" for="writingstyles-name">Send To</label>
<input type="text" id="sendto" class="form-control" name="sendto" value="<?php echo $model['sendto']?>" maxlength="25" aria-required="true" aria-invalid="false">

<div class="help-block"></div>
</div>
    <div class="form-group field-writingstyles-is_active">

<input type="hidden" name="status" <?php if($model['status'] == 0) echo "checked='checked'" ?>  value="0"><label><input type="checkbox" id="is_active" <?php if($model['status'] == 0) echo "checked='checked'" ?> name="status" value="1" checked=""> Active</label>

<div class="help-block"></div>
</div>
    <div class="form-group">
        <button type="submit" class="btn btn-success">Save</button>    </div>

    </form>
</div>

    </div>
</div>
