<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaperTypes */

$this->title = 'Create Paper Types';
$this->params['breadcrumbs'][] = ['label' => 'Paper Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">


        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
