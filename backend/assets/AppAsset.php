<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public function __construct(array $config = [])
    {
        $this->ControlJs(\Yii::$app->controller->module->requestedRoute);
        parent::__construct($config);
    }

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js;


    public function ControlJs($Route)
    {
        $this->js = [
            //'/'.\Yii::$app->params['subfolder'].'/monster-admin/assets/plugins/jquery/jquery.min.js',
            '/'.\Yii::$app->params['subfolder'].'/monster-admin/assets/plugins/bootstrap/js/popper.min.js',
            '/'.\Yii::$app->params['subfolder'].'/monster-admin/assets/plugins/bootstrap/js/bootstrap.min.js',
            '/'.\Yii::$app->params['subfolder'].'/monster-admin/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js',
            '/'.\Yii::$app->params['subfolder'].'/monster-admin/assets/plugins/bootstrap-datepicker/jquery.datetimepicker.full.js',
            '/'.\Yii::$app->params['subfolder'].'/monster-admin/js/jquery.slimscroll.js',
            '/'.\Yii::$app->params['subfolder'].'/monster-admin/js/sidebarmenu.js',
            '/'.\Yii::$app->params['subfolder'].'/monster-admin/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js',
            '/'.\Yii::$app->params['subfolder'].'/monster-admin/js/custom.min.js',
            '/'.\Yii::$app->params['subfolder'].'/monster-admin/js/jasny-bootstrap.js',
            '//cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js',
            '/'.\Yii::$app->params['subfolder'].'/js/jquery.textcomplete.min.js',
            '/'.\Yii::$app->params['subfolder'].'/js/order.js',
            '/'.\Yii::$app->params['subfolder'].'/js/script.js',
        ];



    }

    public $jsOptions =  ['position' => \yii\web\View::POS_END];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}