<?php

namespace backend\controllers;

use common\models\Customers;
use common\models\EmailTemplates;
use common\models\Transactions;
use common\models\User;
use common\models\WriterTasks;
use Yii;
use common\models\Orders;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class OrderController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionView()
    {
        $ordrId = \Yii::$app->request->get('id');
        $order = Orders::find()->where(['id' => $ordrId])->one();
        $customer = Customers::find()->where(['id' => $order->customer_id])->one();
        $inv = Transactions::find()->where(['order_id' => $ordrId])->one();

        $order_attachments = (new \yii\db\Query())
        ->select('order_id,file_path')
        ->from('order_attachments')
        ->where(['order_id' => $ordrId])
        ->all();

        if ($ordrId) {
            return $this->render('view', ['id' => $ordrId, 'o' => $order, 'c' => $customer, 't' => $inv,'order_attachments'=>$order_attachments]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAgentView()
    {
    $ordrId = \Yii::$app->request->get('id');
    $order = Orders::find()->where(['id' => $ordrId])->one();
    $customer = Customers::find()->where(['id' => $order->customer_id])->one();
    $inv = Transactions::find()->where(['order_id' => $ordrId])->one();

    $order_attachments = (new \yii\db\Query())
    ->select('order_id,file_path')
    ->from('order_attachments')
    ->where(['order_id' => $ordrId])
    ->all();

        if ($ordrId) {
            return $this->render('agent-view', ['id' => $ordrId, 'o' => $order, 'c' => $customer, 't' => $inv,'order_attachments'=>$order_attachments]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEdit($id)
    {
        $model = Orders::find()->where(['id' => $id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/site/index']);
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionWriterAssign($id)
    {
        $order = Orders::find()->where(['id' => $id])->one();
        if ($id) {
            if(Yii::$app->request->post())
            {
                $wid = Yii::$app->request->post('writer_id');
                $rate = Yii::$app->request->post('rate_price');
                $wdeadline = Yii::$app->request->post('submission_date');
                $pc = Yii::$app->request->post('payment_cond');
                $currency = $_POST['currency'];
                $order->writer_id = $wid;
                $order->writer_deadline = $wdeadline;
                $order->status = 'in_progress';
                $order->save(false);
                // update rate for writer
                $task = WriterTasks::find()->where(['writer_id'=>$wid,'order_id'=>$id])->one();
                if(!$task)
                $task = new WriterTasks();
                $task->writer_id = $wid;
                $task->order_id  = $id;
                $task->task_rate = $rate;
                $task->currency_id = $currency;
                $task->payment_condition = ($pc == '1') ? 'Fixed' : 'Per Page';
                $task->submission_date = $wdeadline;
                $task->save(false);

                // send email to client for new order
                $et = new EmailTemplates();
                $et->order = $order;
                $et->type = 'task_assign_to_writer';
                $et->sendEmails();
                return $this->redirect(['/site/index']);
            }
            return $this->render('writer-assign', ['id' => $id, 'o' => $order]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionAgentAssign($id)
    {
        $order = Orders::find()->where(['id' => $id])->one();
        if ($id) {
            if(Yii::$app->request->post())
            {
                $aid = Yii::$app->request->post('agent_id');

                $order->agent_id = $aid;
                $order->status = 'pending';
                $order->save(false);
                // send email to client for new order
                $et = new EmailTemplates();
                $et->order = $order;
                $et->customer =  $order->customer;
                $et->type = 'client_intro';
                $et->sendEmails();
                return $this->redirect(['/site/index']);
            }
            return $this->render('agent-assign', ['id' => $id, 'o' => $order]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAgentPick($id)
    {
        $order = Orders::find()->where(['id' => $id])->one();
        if ($id) {
            $aid = Yii::$app->user->id;

            $order->agent_id = $aid;
            $order->status = 'pending';
            $order->save(false);
            // send email to client for new order
            $et = new EmailTemplates();
            $et->order = $order;
            $et->customer =  $order->customer;
            $et->type = 'client_intro';
            $et->sendEmails();
            return $this->redirect(['/site/index']);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAssignUpdate($id)
    {
        $order = Orders::find()->where(['id' => $id])->one();
        if ($id) {
            $task = WriterTasks::find()->where(['order_id'=>$id])->one();
            if(Yii::$app->request->post())
            {
                $wid = Yii::$app->request->post('writer_id');
                $rate = Yii::$app->request->post('rate_price');
                $wdeadline = Yii::$app->request->post('submission_date');
                $pc = Yii::$app->request->post('payment_cond');
                $status = Yii::$app->request->post('status');

                $order->writer_id = $wid;
                $order->writer_deadline = $wdeadline;
                $order->status = $status;
                $order->save(false);
                // update rate for writer
                $task = WriterTasks::find()->where(['writer_id'=>$task->writer_id,'order_id'=>$id])->one();
                $task->writer_id = $wid;
                $task->task_rate = $rate;
                $task->payment_condition = ($pc == '1') ? 'Fixed' : 'Per Page';
                $task->status = $status;
                $task->submission_date = $wdeadline;
                $task->save(false);

                return $this->redirect(['/site/index']);
            }
            return $this->render('update-assign', ['id' => $id, 'o' => $order,'t'=>$task]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPending()
    {
        return $this->render('pending');
    }

    public function actionProcess()
    {
        return $this->render('process');
    }

}
