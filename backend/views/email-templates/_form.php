<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\EmailTemplates */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css">
    /* DROP DOWN */
    .dropdown-menu {
        font-size: 12px;
        -moz-box-shadow: 0px 2px 1px 0px rgba(0, 0, 0, 0.1);
        -webkit-box-shadow: 0px 2px 1px 0px rgba(0, 0, 0, 0.1);
        box-shadow: 0px 2px 1px 0px rgba(0, 0, 0, 0.1);
        border: 1px solid #E5E5E5;
        padding: 0px;
        margin: 8px 0px 0px;
    }
    .dropdown-menu > li > a {
        padding: 8px 15px;
        border-bottom: 1px solid #E9E9E9;
        line-height: 20px;
    }
    .dropdown-menu > li:last-child > a {
        border-bottom: 0px;
    }
    .dropdown-menu:after,
    .dropdown-menu:before {
        bottom: 100%;
        left: 50%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
    }
    .dropdown-menu:after {
        border-color: rgba(255, 255, 255, 0);
        border-bottom-color: #FFF;
        border-width: 5px;
        margin-left: -5px;
    }
    .dropdown-menu:before {
        border-color: rgba(229, 229, 229, 0);
        border-bottom-color: #E5E5E5;
        border-width: 6px;
        margin-left: -6px;
    }
    .dropup .dropdown-menu:after,
    .dropup .dropdown-menu:before {
        display: none;
    }
    .dropdown-header {
        padding: 10px 15px;
        border-bottom: 1px solid #E5E5E5;
        color: #777;
    }
    .dropdown-menu .fa {
        margin-right: 5px;
    }
    .dropdown-menu .divider {
        background: #F5F5F5;
        margin: 0px;
        padding: 15px 0px 0px;
        border-bottom: 1px solid #E5E5E5;
    }
    /* EOF DROP DOWN */
</style>
<div class="email-templates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList([ 'task_complete' => 'Task complete', 'client_intro' => 'Client intro', 'order_not_paid' => 'Order not paid', 'task_assign_to_writer' => 'Task assign to writer', 'task_notification_csr' => 'Task notification csr', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => 150]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs("
           jQuery('textarea,#emailtemplates-subject').textcomplete([
           {
              mentions: ['client_name', 'task_topic', 'no_pages','customer_deadline','amount', 'order_no', 'agent_name','writer_name','subject_area','writer_deadline'],
               match: /\B#(\w*)$/,
               search: function (term, callback) {
                   callback($.map(this.mentions, function (mention) {
                       return mention.indexOf(term) === 0 ? mention : null;
                   }));
               },
               index: 1,
               replace: function (mention) {
                   return '#' + mention ;
               }
           }
       ], {appendTo: 'body'}).overlay([
           {
               match: /\B#\w+/g,
               css: {
                   'background-color': '#d8dfea'
               }
           }
       ]);
   ");
?>