<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fixed_links".
 *
 * @property string $id
 * @property string $title
 * @property string $order_id
 * @property string $unique_code
 * @property double $amount
 * @property double $discount_percentage
 * @property double $discount_amount
 * @property int $show_discount_msg
 * @property string $status
 * @property string $expiry_date
 * @property string $paper_type_id
 * @property string $service_type_id
 * @property string $urgency_id
 * @property string $quality_level_id
 * @property string $no_pages_id
 * @property string $currency
 * @property string $created_at
 *
 * @property Orders $order
 * @property PaperTypes $paperType
 * @property ServiceTypes $serviceType
 * @property Urgency $urgency
 * @property QualityLevels $qualityLevel
 * @property NoPages $noPages
 */
class FixedLinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fixed_links';
    }

    public $to_charge = 0;
    public $discount = "";

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'paper_type_id', 'service_type_id', 'urgency_id', 'quality_level_id', 'no_pages_id'], 'integer'],
            [['title','unique_code', 'amount', 'expiry_date', 'paper_type_id', 'service_type_id', 'urgency_id', 'quality_level_id', 'no_pages_id', 'currency', 'created_at'], 'required'],
            [['amount', 'discount_percentage', 'discount_amount'], 'number'],
            [['status'], 'string'],
            [['expiry_date', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 250],
            [['unique_code'], 'string', 'max' => 16],
            [['show_discount_msg'], 'string', 'max' => 1],
            [['currency'], 'string', 'max' => 25],
            [['unique_code'], 'unique'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['paper_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaperTypes::className(), 'targetAttribute' => ['paper_type_id' => 'id']],
            [['service_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceTypes::className(), 'targetAttribute' => ['service_type_id' => 'id']],
            [['urgency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Urgency::className(), 'targetAttribute' => ['urgency_id' => 'id']],
            [['quality_level_id'], 'exist', 'skipOnError' => true, 'targetClass' => QualityLevels::className(), 'targetAttribute' => ['quality_level_id' => 'id']],
            [['no_pages_id'], 'exist', 'skipOnError' => true, 'targetClass' => NoPages::className(), 'targetAttribute' => ['no_pages_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'order_id' => 'Order #',
            'unique_code' => 'Unique Code',
            'amount' => 'Actual',
            'discount_percentage' => 'Discount Percentage',
            'discount_amount' => 'Discount Amount',
            'show_discount_msg' => 'Show Discount Msg',
            'status' => 'Status',
            'expiry_date' => 'Expiry',
            'paper_type_id' => 'Paper Type',
            'service_type_id' => 'Service Type',
            'urgency_id' => 'Urgency',
            'quality_level_id' => 'Quality Level',
            'no_pages_id' => 'No Pages',
            'currency' => 'Currency',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaperType()
    {
        return $this->hasOne(PaperTypes::className(), ['id' => 'paper_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceType()
    {
        return $this->hasOne(ServiceTypes::className(), ['id' => 'service_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrgency()
    {
        return $this->hasOne(Urgency::className(), ['id' => 'urgency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQualityLevel()
    {
        return $this->hasOne(QualityLevels::className(), ['id' => 'quality_level_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoPages()
    {
        return $this->hasOne(NoPages::className(), ['id' => 'no_pages_id']);
    }

    public function beforeSave($insert)
    {
        if(Yii::$app->request->post('fixed_or_percentage') == '2')
            $this->discount_percentage = Yii::$app->request->post('percentage_discount_percentage');
        else
            $this->discount_amount = Yii::$app->request->post('fixed_discount_amount');
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterFind()
    {
        $this->to_charge = "0.00";
        $this->discount = "";
        if($this->discount_percentage != '')
        {
            $this->to_charge = $this->amount - ($this->amount * ($this->discount_percentage / 100));
            $this->to_charge = number_format($this->to_charge,2);
            $this->discount = ($this->amount * ($this->discount_percentage / 100)). " (".$this->discount_percentage."%) OFF";
        } else {
            $this->to_charge = $this->amount - $this->discount_amount;
            $this->to_charge = number_format($this->to_charge,2);
            $this->discount =  "FLAT (".$this->discount_amount.") OFF";
        }
        parent::afterFind(); // TODO: Change the autogenerated stub
    }
}
