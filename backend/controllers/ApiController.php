<?php
/**
 * Created by PhpStorm.
 * User: umerz
 * Date: 10/6/2018
 * Time: 4:02 AM
 */

namespace backend\controllers;


use common\models\Customers;
use common\models\Orders;
use common\models\Transactions;
use common\models\User;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;
use yii\helpers\Json;

class ApiController  extends Controller
{

    public function actionRecord()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->_checkAuth();
            switch($_POST['model'])
            {
                // Get an instance of the respective model
                case 'order':
                    $custId = $this->_createCustomer();
                    $model = new Orders();
                    $model->customer_id = $custId;
                    $model->status = 'new';
                    $model->order_date = date('Y-m-d h:i:s');
                    $model->customer_deadline = \backend\util\Help::getCustomerDealLine($_POST['urgency_id']);

                    break;
                case 'transaction':
                    $orderInfo  = $this->_getOrderInfoByEmail();
                    $model = new Transactions();
                    $model->payment_date = date('Y-m-d h:i:s');
                    break;


            }
            // Try to assign POST values to attributes
            unset($_POST['cust']);
            foreach($_POST as $var=>$value) {
                // Does the model have this attribute? If not raise an error
                if($model->hasAttribute($var)) {
                    $model->$var = $value;
                }

            }


            // Try to save the model
            if($model->save(false))
                $this->_sendResponse(200, Json::encode($model));
            else {
                // Errors occurred
                $msg = "<h1>Error</h1>";
                $msg .= sprintf("Couldn't create model <b>%s</b>", $_POST['model']);
                $msg .= "<ul>";
                foreach($model->errors as $attribute=>$attr_errors) {
                    $msg .= "<li>Attribute: $attribute</li>";
                    $msg .= "<ul>";
                    foreach($attr_errors as $attr_error)
                        $msg .= "<li>$attr_error</li>";
                    $msg .= "</ul>";
                }
                $msg .= "</ul>";
                $this->_sendResponse(500, $msg );
            }

        } else {
            throw new NotFoundHttpException('only post params required');
        }
    }

    private function _getOrderInfoByEmail()
    {
        $email = $_POST['email'];
        if(!isset($email))
        {
            throw new NotFoundHttpException('email not found');
        } else {
            $cust = Customers::find()->where(['email'=>$email]);
            if($cust)
            {


            } else {
                $msg = "<h1>Error</h1>";
                $msg .= sprintf("Couldn't find customer with email<b>%s</b>", $_POST['email']);

                $this->_sendResponse(500, $msg );
            }

        }
    }

    private function _createCustomer()
    {
        $cust = new Customers();
        $cust->created_at = date('Y-m-d h:i:s');
        foreach($_POST['cust'] as $var=>$value) {
            // Does the model have this attribute? If not raise an error
            if($cust->hasAttribute($var)) {
                $cust->$var = $value;
            }
        }

        if($cust->save(false))
            return $cust->id;
        else {
            // Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", $_POST['model']);
            $msg .= "<ul>";
            foreach($cust->errors as $attribute=>$attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";
            $this->_sendResponse(500, $msg );
        }
    }

    private function _checkAuth() {
        // Check if we have the USERNAME and PASSWORD HTTP headers set?
        if(!(isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW']))) {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];
        // Find the user
        $user = User::findByUsername(strtolower($username));

        if($user===null) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: Username or password is invalid');
        } else if(!$user->validatePassword($password)) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: Username or Password is invalid');
        }
    }

    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if($body != '')
        {
            // send the body
            echo $body;
        }
        // we need to create the body if none is passed
        else
        {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch($status)
            {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templated in a real-world solution
            $body = '
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
                </head>
                <body>
                    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
                    <p>' . $message . '</p>
                    <hr />
                    <address>' . $signature . '</address>
                </body>
                </html>';

            echo $body;
        }
        \Yii::$app->end();
    }

    private function _getStatusCodeMessage($status) {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

}