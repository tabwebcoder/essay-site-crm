<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AcademicLevelsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Academic Levels';
$this->params['breadcrumbs'][] = $this->title;
?><div class="card">
    <div class="card-body">

        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('Add Academic Levels', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                ['attribute' => 'is_active', 'filter' => false, 'format' => 'boolean'],
                ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}',],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
    </div>
<?php
$this->registerJs("
jQuery(document).ready(function () {
        jQuery('.grid-view').find('table').removeClass('table-striped table-bordered').addClass('color-bordered-table muted-bordered-table');
    })
");
?>