<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DiscountCodes */

$this->title = Yii::t('app', 'Create Discount Codes');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Discount Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    </div>
</div>
