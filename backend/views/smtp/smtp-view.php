<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'SMTP Setting View';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">
        <h4 class="card-title">SMTP Setting Information</h4>
        <table class="table color-bordered-table success-bordered-table">
            <tr>
                <th>Host</th>
                <td><?=$smtp['host']?></td>
            </tr>
            <tr>
                <th>UserName</th>
                <td><?=$smtp['username']?></td>
            </tr>
                <th>Password</th>
                <td><?=$smtp['password']?></td>
            </tr>
            <tr>
                <th>Port</th>
                <td><?=$smtp['Port']?></td>
            </tr>
            <tr>
                <th>SMTP Secure</th>
                <td><?=$smtp['SMTPSecure']?></td>
            </tr>
            <tr>
                <th>Send From</th>
                <td><?=$smtp['sendfrom']?></td>
            </tr>
            <tr>
                <th>Send To</th>
                <td><?=$smtp['sendto']?></td>
            </tr>
 
        </table>
        <a href="/<?= \Yii::$app->params['preLink'] ?>smtp/edit/?id=<?=$smtp['id']?>" class="btn btn-success">Edit</a>

    </div>
</div>

