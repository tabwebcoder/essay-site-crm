<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FixedLinks */
/* @var $form yii\widgets\ActiveForm */
$paperTypeList = ArrayHelper::map(\common\models\PaperTypes::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$serviceTypeList = ArrayHelper::map(\common\models\ServiceTypes::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$urgencyList = ArrayHelper::map(\common\models\Urgency::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$qualityList = ArrayHelper::map(\common\models\QualityLevels::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$pagesList = ArrayHelper::map(\common\models\NoPages::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$currenciesList = ArrayHelper::map(\common\models\Currencies::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
?>
<div class="fixed-links-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true,'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'unique_code')->textInput(['maxlength' => true,'disabled'=>'disabled']) ?>

    <?= $form->field($model, 'paper_type_id')->dropDownList($paperTypeList,['prompt'=>'Select Paper Type','disabled'=>'disabled']);?>

    <?= $form->field($model, 'service_type_id')->dropDownList($serviceTypeList,['prompt'=>'Select Service Type','disabled'=>'disabled']);?>

    <?= $form->field($model, 'urgency_id')->dropDownList($urgencyList,['prompt'=>'Select Urgency','disabled'=>'disabled']);?>

    <?= $form->field($model, 'quality_level_id')->dropDownList($qualityList,['prompt'=>'Select Quality Level','disabled'=>'disabled']);?>

    <?= $form->field($model, 'no_pages_id')->dropDownList($pagesList,['prompt'=>'Select Num of Pages','disabled'=>'disabled']);?>

    <?= $form->field($model, 'currency')->dropDownList($currenciesList,['disabled'=>'disabled']);?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <div id="discount-section">
        <div class="card">
            <label>Discount Type: <span style="font-weight: bold"><?=($model->discount_percentage) ? 'Percentage':'Fixed Amount'?></span> </label>
            <label>Discount Percentage: <span style="font-weight: bold"><?=$model->discount_percentage . '%'?></span> </label>
            <label>Discount Amount: <span style="font-weight: bold"><?=$model->discount_amount?></span> </label>
            <label>To Charge Amount: <span style="font-weight: bold"><?=$model->to_charge?></span> </label>
        </div>
    </div>

    <?= $form->field($model, 'show_discount_msg')->checkbox(['class'=>'','disabled'=>'disabled']) ?>
    <?= $form->field($model, 'expiry_date')->textInput(['class'=>'form-control dt','disabled'=>'disabled']) ?>



    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs(
    "
    $(function () {
         $('.dt').datetimepicker();
     });"
)

?>