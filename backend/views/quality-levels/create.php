<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\QualityLevels */

$this->title = 'Create Quality Levels';
$this->params['breadcrumbs'][] = ['label' => 'Quality Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">


        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>

