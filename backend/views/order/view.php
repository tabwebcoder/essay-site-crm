<?php

/* @var $this yii\web\View */

use yii\helpers\Html;





$this->title = 'Order View #'.$id;

$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['/site/index']];

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="card">

    <div class="card-body">

        <h4 class="card-title">Basic Order Information</h4>

        <table class="table color-bordered-table success-bordered-table">

            <tr>

                <th>Order Id</th>

                <td><?=$o->id?></td>

            </tr>

            <tr>

                <th>Paper Type</th>

                <td><?=$o->paperType->name?></td>

            </tr>

                <th>Service Type</th>

                <td><?=$o->serviceType->name?></td>

            </tr>

            <tr>

                <th>Order Date</th>

                <td><?=$o->order_date?></td>

            </tr>

            <tr>

                <th>Urgency</th>

                <td><?=$o->urgency->name?></td>

            </tr>

            <tr>

                <th>Customer Deadline</th>

                <td><?=\backend\util\Help::getCustomerDealLine($o->urgency_id)?></td>

            </tr>

            <tr>

                <th>Quality</th>

                <td><?=$o->quality->name?></td>

            </tr>

            <tr>

                <th>Num of Pages</th>

                <td><?=$o->noPages->name?></td>

            </tr>

            <tr>

                <th>Subject</th>

                <td><?=$o->subjectArea->name?></td>

            </tr>

            <tr>

                <th>Topic</th>

                <td><?=$o->topic?></td>

            </tr>

            <tr>

                <th>Level</th>

                <td><?=$o->acadamicLevel->name?></td>

            </tr>

            <tr>

                <th>Writing Style</th>

                <td><?=$o->writingStyle->name?></td>

            </tr>

            <tr>

                <th>Language</th>

                <td><?=$o->language?></td>

            </tr>

            <tr>

                <th>Description</th>

                <td><?=$o->instructions?></td>

            </tr>





        </table>

        <div class="form-group">

            <?= Html::a('Task Assigning Detail', ['/order/writer-assign','id'=>$o->id], ['class'=>'btn btn-success btn-size-small']) ?>

            <?= Html::a('Edit this Order', ['/order/edit','id'=>$o->id], ['class'=>'btn btn-success btn-size-small']) ?>

        </div>

    </div>

</div>

<div class="card">
    <div class="card-body">
    <h4 class="card-title">Order Attachments - Order # <?=$id?></h4>
        <table class="table color-bordered-table success-bordered-table">
            <?php
            $attachment = 1;
            if(count($order_attachments) > 0){
            foreach($order_attachments as $key=>$order_attachment){ ?>
            <tr>
                <th>Attachment # <?=$attachment?></th>
                <td><a target="_blank" href="<?=$order_attachment['file_path']?>"><?=$order_attachment['file_path']?></a></td>
            </tr>
            <?php
            $attachment++;
            } 
            }else{
                ?>
                No Attachments
                <?php
            }?>

        </table>

    </div>
</div>

<div class="card">

    <div class="card-body">

    <h4 class="card-title">Client Information - Order # <?=$id?></h4>

        <table class="table color-bordered-table success-bordered-table">

            <tr>

                <th>Client Id</th>

                <td><?=$c->id?></td>

            </tr>

            <tr>

                <th>Customer Name</th>

                <td><?=$c->name?></td>

            </tr>

                <th>Customer Email</th>

                <td><?=$c->email?></td>

            </tr>

            <tr>

                <th>Customer Phone</th>

                <td><?=$c->phone?></td>

            </tr>

            <tr>

                <th>Customer IP</th>

                <td><?=$c->ip?></td>

            </tr>

            <tr>

                <th>Customer IP-City</th>

                <td><?=$c->city?></td>

            </tr>

            <tr>

                <th>Customer IP-Region</th>

                <td><?=$c->region?></td>

            </tr>

            <tr>

                <th>Customer IP-Country</th>

                <td><?=$c->country?></td>

            </tr>

            <tr>

                <th>Customer Registration </th>

                <td><?=$c->created_at?></td>

            </tr>





        </table>



    </div>

</div>

<?php if($t):?>

<div class="card">

    <div class="card-body">

        <h4 class="card-title">Payment Information - Order # <?=$id?></h4>

        <table class="table color-bordered-table success-bordered-table">

            <thead>

            <tr>

                <th>Invoice ID</th>

                <th>Method</th>

                <th>Transaction Ref</th>

                <th>Payee Name</th>

                <th>Pay Date</th>

                <th>Status</th>

                <th>Amount</th>

                <th>Action</th>

            </tr>

            </thead>

            <tbody>

            <tr>

                <td><?=$t->id?></td>

                <td><?=$t->payment_method?></td>

                <td><?=$t->transaction_id?></td>

                <td><?=$t->payee_name?></td>

                <td><?=$t->payment_date?></td>

                <td><span class="badge badge-<?=($t->status == 'Paid')?'success':'warning'?>"><?=$t->status?></span></td>

                <td><?=$t->payee_amount?></td>



            </tr>

            </tbody>

        </table>

    </div>

</div>

<?php endif; ?>